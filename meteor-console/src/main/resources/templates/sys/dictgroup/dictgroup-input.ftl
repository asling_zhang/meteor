<#--
/****************************************************
 * Description: 字典组管理的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">

<@input url="${base}/sys/dictgroup/save" id=tabId>
   <input type="hidden" name="id" value="${dictgroup.id}"/>

   <@formgroup title='组编码'>
               <input type="text" name="groupCodeM" value="${dictgroup.groupCodeM}" check-type="required">

   </@formgroup>
   <@formgroup title='组名'>
               <input type="text" name="groupName" value="${dictgroup.groupName}" >

   </@formgroup>
</@input>
