<#--
/****************************************************
 * Description: 私钥信息的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-13 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@input url="${base}/meteor/rsa/save" id=tabId>
    <input type="hidden" name="id" value="${rsa.id}"/>
    <@formgroup title='私钥名称'>
        <input type="text" name="rsaName" value="${rsa.rsaName}" check-type="required">
    </@formgroup>
    <@formgroup title='私钥内容'>
        <textarea name="rsaValue" value="${rsa.rsaValue}" check-type="required"></textarea>
    </@formgroup>
    <@formgroup title='私钥用户'>
        <input type="text" name="rsaUsername" value="${rsa.rsaUsername}" check-type="required">
    </@formgroup>
    <@formgroup title='私钥密码'>
        <input type="password" name="rsaPassword" value="${rsa.rsaPassword}">
    </@formgroup>
    <@formgroup title='状态'>
        <@swichInForm name="status" val=rsa.status onTitle="有效" offTitle="无效"></@swichInForm>
    </@formgroup>
    <@formgroup title='创建时间'>
        <@datetime name="createTime" dateValue=rsa.createTime required="required" default=true/>
    </@formgroup>
    <@formgroup title='创建人员ID'>
        <input type="text" name="createPersonId"
               value="<#if (rsa.createPersonId)?? && rsa.createPersonId!=''>${rsa.createPersonId}<#else>${session_manager_info_key.userId}</#if>"
               check-type="required" readonly="readonly">
    </@formgroup>
    <@formgroup title='创建人员姓名'>
        <input type="text" name="createPersonName"
               value="<#if (rsa.createPersonName)?? && rsa.createPersonName!=''>${rsa.createPersonName}<#else>${session_manager_info_key.userName}</#if>"
               check-type="required" readonly="readonly">
    </@formgroup>
</@input>
