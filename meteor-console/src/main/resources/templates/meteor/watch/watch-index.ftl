<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<#include "/templates/xjj-list.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <input type="hidden" id="watch-className">
        <input type="hidden" id="watch-method">
        <@querygroup title='监控时长(秒)'>
            <input type="text" id="watch-time" value="10" class="form-control input-sm" placeholder="最大30秒"
                   check-type='required number'>
        </@querygroup>
        <@button type="info" onclick="sendWatchCmd()">重试</@button>
    </@query>

</@content>

<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="watch-console-card">
            <div id="watch-console"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var watch_ws = null;

    initWatchConsole();

    function initWatchConsole() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }
        watch_ws = initArthasConsole("watch-console", path, "meteor_watch", true);
    }

    function sendWatchCmd(className, method) {
        if (className != null) {
            $("#watch-className").val(className);
        }
        if (method != null) {
            $("#watch-method").val(method);
        }

        var className_cmd = $("#watch-className").val();
        var method_cmd = $("#watch-method").val();


        var cmd = "watch  " + className_cmd + " " + method_cmd + " \"{params,target,throwExp,returnObj}\" -x 2 -b -s";
        watch_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
        //防止数据量过大，影响服务器
        var time = $("#watch-time").val();
        if (time > 30) {
            time = 30;
        }
        setTimeout(function () {
            watch_ws.send(JSON.stringify({action: 'read', data: ""}));
        }, time * 1000);
    }

</script>
