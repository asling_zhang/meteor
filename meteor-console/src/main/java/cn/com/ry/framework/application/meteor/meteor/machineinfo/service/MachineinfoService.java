/****************************************************
 * Description: Service for 服务器信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-04 reywong Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.meteor.machineinfo.service;

import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoEntity;

import java.util.List;
import java.util.Map;


public interface MachineinfoService extends XjjService<MachineinfoEntity> {

    /**
     * 导入
     *
     * @param fileId
     */
    Map<String, Object> saveImport(Long fileId) throws ValidationException;

    List<String> findModuleList(Long userId);

    List<MachineinfoEntity> findHostListByModule(Long userId, String moduleName);

    MachineinfoEntity findMachineByMachineId(Long userId, Long machineId, String moduleName);
}
