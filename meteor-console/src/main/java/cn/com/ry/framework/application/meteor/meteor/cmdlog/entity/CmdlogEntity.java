/****************************************************
 * Description: Entity for t_meteor_cmdlog
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-05 reywong Create File
**************************************************/

package cn.com.ry.framework.application.meteor.meteor.cmdlog.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import cn.com.ry.framework.application.meteor.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CmdlogEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public CmdlogEntity(){}
    private String hostname;//服务器地址
    private String appname;//应用名称
    private String cmd;//命令
    private String createPersonId;//操作人员ID
    private String createPersonName;//操作人员名称
@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    /**
     * 返回服务器地址
     * @return 服务器地址
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * 设置服务器地址
     * @param hostname 服务器地址
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * 返回应用名称
     * @return 应用名称
     */
    public String getAppname() {
        return appname;
    }

    /**
     * 设置应用名称
     * @param appname 应用名称
     */
    public void setAppname(String appname) {
        this.appname = appname;
    }

    /**
     * 返回命令
     * @return 命令
     */
    public String getCmd() {
        return cmd;
    }

    /**
     * 设置命令
     * @param cmd 命令
     */
    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    /**
     * 返回操作人员ID
     * @return 操作人员ID
     */
    public String getCreatePersonId() {
        return createPersonId;
    }

    /**
     * 设置操作人员ID
     * @param createPersonId 操作人员ID
     */
    public void setCreatePersonId(String createPersonId) {
        this.createPersonId = createPersonId;
    }

    /**
     * 返回操作人员名称
     * @return 操作人员名称
     */
    public String getCreatePersonName() {
        return createPersonName;
    }

    /**
     * 设置操作人员名称
     * @param createPersonName 操作人员名称
     */
    public void setCreatePersonName(String createPersonName) {
        this.createPersonName = createPersonName;
    }

    /**
     * 返回创建时间
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cn.com.ry.framework.application.meteor.meteor.cmdlog.entity.CmdlogEntity").append("ID="+this.getId()).toString();
    }
}

