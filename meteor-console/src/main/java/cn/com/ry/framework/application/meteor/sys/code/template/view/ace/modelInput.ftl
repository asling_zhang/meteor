${'<#--'}
/****************************************************
 * Description: ${model.label}的输入页面，包括添加和修改
 * Copyright:   Copyright (c) ${model.year}
 * Company:     ${model.company}
 * @author      ${model.author}
 * @version     ${model.version}
 * @see
	HISTORY
    *  ${model.date} ${model.author} Create File
**************************************************/
${'-->'}
<${'#'}include "/templates/xjj-index.ftl">
<${'@'}input url="${'$'}{base}${model.requestMapping}/save" id=tabId>
   <input type="hidden" name="id" value="${'$'}{${model.name?uncap_first}.id}"/>
<#list model.fields as field>
    <#if (field.columnDefault)??>
    <#assign defaultValue = "<${'#'}if (${model.name?uncap_first}.${field.propName?uncap_first})?? && ${model.name?uncap_first}.${field.propName?uncap_first}!=''>${'$'}{${model.name?uncap_first}.${field.propName?uncap_first}}<${'#'}else>${field.columnDefault}</${'#'}if>"/>
    <#elseif field.propName?lower_case=='createpersonname'>
    <#assign defaultValue = "<${'#'}if (${model.name?uncap_first}.${field.propName?uncap_first})?? && ${model.name?uncap_first}.${field.propName?uncap_first}!=''>${'$'}{${model.name?uncap_first}.${field.propName?uncap_first}}<${'#'}else>${'$'}{session_manager_info_key.userName}</${'#'}if>"/>
    <#elseif field.propName?lower_case=='createpersonid'>
    <#assign defaultValue = "<${'#'}if (${model.name?uncap_first}.${field.propName?uncap_first})?? && ${model.name?uncap_first}.${field.propName?uncap_first}!=''>${'$'}{${model.name?uncap_first}.${field.propName?uncap_first}}<${'#'}else>${'$'}{session_manager_info_key.userId}</${'#'}if>"/>
    <#else>
    <#assign defaultValue = "${'$'}{${model.name?uncap_first}.${field.propName?uncap_first}}"/>
    </#if>
    <#if field.propName!="id">
    <${'@'}formgroup title='${field.columnComment}'>
    <#if field.propType=="Date">
	<${'@'}datetime name="${field.propName?uncap_first}" dateValue=${model.name?uncap_first}.${field.propName?uncap_first} <#if field.required>required="required"</#if> default=true/>
    <#else>
    <#if field.propName?lower_case =="status">
    <${'@'}swichInForm name="status" val=${model.name?uncap_first}.${field.propName?uncap_first} onTitle="有效" offTitle="无效"></${'@'}swichInForm>
    <#elseif field.propName?lower_case?index_of("password")!=-1>
    <input type="password" name="${field.propName?uncap_first}" value="${defaultValue}" <#if field.checkType!="">check-type="${field.checkType}"</#if>>
    <#elseif field.propName?lower_case=='createpersonname' || field.propName?lower_case=='createpersonid'>
    <input type="text" name="${field.propName?uncap_first}" value="${defaultValue}" <#if field.checkType!="">check-type="${field.checkType}"</#if> readonly="readonly">
    <#else>
    <input type="text" name="${field.propName?uncap_first}" value="${defaultValue}" <#if field.checkType!="">check-type="${field.checkType}"</#if>>
    </#if>
    </#if>
    </${'@'}formgroup>
    </#if>
</#list>
</${'@'}input>
