/****************************************************
 * Description: Service for 数据字典
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sys.dict.service;

import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.sys.dict.entity.DictEntity;


public interface DictService  extends XjjService<DictEntity>{


}
