package cn.com.ry.framework.application.meteor.sys.code.web;

import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecFunction;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.sys.code.CodeFilePropertyConfigLoader;
import cn.com.ry.framework.application.meteor.sys.code.GCConfig;
import cn.com.ry.framework.application.meteor.sys.code.entity.ColumnInfo;
import cn.com.ry.framework.application.meteor.sys.code.entity.TableInfo;
import cn.com.ry.framework.application.meteor.sys.code.generator.GCGenerator;
import cn.com.ry.framework.application.meteor.sys.code.generator.GCModel;
import cn.com.ry.framework.application.meteor.sys.code.service.CodeService;
import cn.com.ry.framework.application.meteor.framework.utils.StringUtils;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Controller
@RequestMapping("/sys/code")
public class CodeController extends SpringControllerSupport {

    @Autowired
    private CodeService codeService;

    @SecPrivilege(title = "代码生成管理")
    @RequestMapping(value = "/index")
    public String generate(Model model) {
        List<String> tableList = codeService.findTableList();
        model.addAttribute("tableList", tableList);
        Properties properties = CodeFilePropertyConfigLoader.load();
        Map<String, String> codeParam = new HashMap<String, String>();
        //目标路径
        codeParam.put("codePath", (String) properties.get("code.dir.base"));
        //项目名称
        codeParam.put("projectName", (String) properties.get("info.project.name"));
        //代码包名
        codeParam.put("globalPackage", (String) properties.get("code.package.name"));
        model.addAttribute("code", codeParam);
        return getViewPath("index");
    }


    /**
     * 代码生成成功
     *
     * @param tables
     * @return
     */
    @SecFunction(code = "generate", title = "代码生成")
    @RequestMapping("/generate")
    public @ResponseBody
    XjjJson generate(@RequestParam(required = false, value = "tables") String[] tables,
                     @RequestParam("codePath") String codePath,
                     @RequestParam("globalPackage") String globalPackage,
                     @RequestParam("projectName") String projectName,
                     @RequestParam("diffTable") String diffTable,
                     @RequestParam("tablePre") String tablePre,
                     @RequestParam("template") String template,
                     @RequestParam("update") String update) {

        if (tables == null || tables.length == 0) {
            return XjjJson.error("请选择要生成代码的数据库表");
        }
        //参数初始化
        if (!StringUtils.isBlank(codePath)) {
            GCConfig.DIR_BASE = codePath;
        }

        String tableName = null;
        for (int i = 0; i < tables.length; i++) {
            TableInfo tableInfo = codeService.getTableInfoByName(tables[i]);
            List<ColumnInfo> columnList = codeService.findColumnsByTable(tables[i]);
            tableName = tableInfo.getTableName();

            GCModel model = GCModel.initGCModel(diffTable, globalPackage, projectName, template, tableName, tablePre, tableInfo.getTableComment(),update, columnList);

            GCGenerator.generateCode(model);
        }
        return XjjJson.success("保存成功");
    }

}
