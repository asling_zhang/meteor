/****************************************************
 * Description: Controller for 字典组管理
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
	*  2019-08-16 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sys.dictgroup.web;

import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.*;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import cn.com.ry.framework.application.meteor.sys.dictgroup.entity.DictgroupEntity;
import cn.com.ry.framework.application.meteor.sys.dictgroup.service.DictgroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/sys/dictgroup")
public class DictgroupController extends SpringControllerSupport{
	@Autowired
	private DictgroupService dictgroupService;


	@SecPrivilege(title="字典组管理管理")
	@RequestMapping(value = "/index")
	public String index(Model model) {
		String page = this.getViewPath("index");
		return page;
	}

	@SecList
	@RequestMapping(value = "/list")
	public String list(Model model,
			@QueryParameter XJJParameter query,
			@ModelAttribute("page") Pagination page
			) {
		page = dictgroupService.findPage(query,page);
		return getViewPath("list");
	}

	@SecCreate
	@RequestMapping("/input")
	public String create(@ModelAttribute("dictgroup") DictgroupEntity dictgroup,Model model){
		return getViewPath("input");
	}

	@SecEdit
	@RequestMapping("/input/{id}")
	public String edit(@PathVariable("id") Long id, Model model){
		DictgroupEntity dictgroup = dictgroupService.getById(id);
		model.addAttribute("dictgroup",dictgroup);
		return getViewPath("input");
	}

	@SecCreate
	@SecEdit
	@RequestMapping("/save")
	public @ResponseBody XjjJson save(@ModelAttribute DictgroupEntity dictgroup){

		validateSave(dictgroup);
		if(dictgroup.isNew())
		{
			//dictgroup.setCreateDate(new Date());
			dictgroupService.save(dictgroup);
		}else
		{
			dictgroupService.update(dictgroup);
		}
		return XjjJson.success("保存成功");
	}


	/**
	 * 数据校验
	 **/
	private void validateSave(DictgroupEntity dictgroup){
		//必填项校验
		// 判断组编码是否为空
		if(null==dictgroup.getGroupCodeM()){
			throw new ValidationException("校验失败，组编码不能为空！");
		}
	}

	@SecDelete
	@RequestMapping("/delete/{id}")
	public @ResponseBody XjjJson delete(@PathVariable("id") Long id){
		dictgroupService.delete(id);
		return XjjJson.success("成功删除1条");
	}
	@SecDelete
	@RequestMapping("/delete")
	public @ResponseBody XjjJson delete(@RequestParam("ids") Long[] ids){
		if(ids == null || ids.length == 0){
			return XjjJson.error("没有选择删除记录");
		}
		for(Long id : ids){
			dictgroupService.delete(id);
		}
		return XjjJson.success("成功删除"+ids.length+"条");
	}
}

