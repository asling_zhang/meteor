/****************************************************
 * Description: ServiceImpl for 字典组管理
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sys.dictgroup.service.impl;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.framework.service.XjjServiceSupport;
import cn.com.ry.framework.application.meteor.sys.dictgroup.dao.DictgroupDao;
import cn.com.ry.framework.application.meteor.sys.dictgroup.entity.DictgroupEntity;
import cn.com.ry.framework.application.meteor.sys.dictgroup.service.DictgroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DictgroupServiceImpl extends XjjServiceSupport<DictgroupEntity> implements DictgroupService {

	@Autowired
	private DictgroupDao dictgroupDao;

	@Override
	public XjjDAO<DictgroupEntity> getDao() {

		return dictgroupDao;
	}
}
