/****************************************************
 * Description: DAO for t_meteor_cmdlog
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-05 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.meteor.cmdlog.dao;

import cn.com.ry.framework.application.meteor.meteor.cmdlog.entity.CmdlogEntity;
import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;


public interface CmdlogDao  extends XjjDAO<CmdlogEntity> {

}

