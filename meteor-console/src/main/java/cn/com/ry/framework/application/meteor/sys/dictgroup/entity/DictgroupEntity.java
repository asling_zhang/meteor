/****************************************************
 * Description: Entity for 字典组管理
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sys.dictgroup.entity;

import cn.com.ry.framework.application.meteor.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class DictgroupEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public DictgroupEntity(){}
    private String groupCodeM;//组编码
    private String groupName;//组名
    /**
     * 返回组编码
     * @return 组编码
     */
    public String getGroupCodeM() {
        return groupCodeM;
    }

    /**
     * 设置组编码
     * @param groupCodeM 组编码
     */
    public void setGroupCodeM(String groupCodeM) {
        this.groupCodeM = groupCodeM;
    }

    /**
     * 返回组名
     * @return 组名
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * 设置组名
     * @param groupName 组名
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cn.com.ry.framework.application.oil.sys.dictgroup.entity.DictgroupEntity").append("ID="+this.getId()).toString();
    }
}

